package teste.simova.ex2;

import teste.simova.utils.Order;
import teste.simova.utils.OrderItem;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/3/13
 * Time: 8:02 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet(name="simple-servlet", urlPatterns = { "/*" })
public class SimpleServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String url = request.getParameter("url");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new URL(url).openStream()));


        Order order = teste.simova.utils.FileReader.parse(bufferedReader);
        printOrder(order, response.getWriter());

    }

    public static void printOrder(Order order, PrintWriter writer) {
        writer.println(String.format("Nome: %s", order.getName()));
        writer.println(String.format("CPF: %s", order.getCpf()));
        writer.println(String.format("Tel: %s", order.getPhone()));

        for (int i = 0; i < order.getItems().size(); i++){
            OrderItem oi = order.getItems().get(i);
            writer.println(String.format("\t %d. Item:%s - R$ %.2f", i, oi.getName(), oi.getPrice().doubleValue()));
        }

        writer.println(String.format("\n\nTotal: R$ %.2f", order.getPrice().doubleValue()));
    }
}
