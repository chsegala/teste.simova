package teste.simova.ex5.core.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

import org.springframework.security.core.GrantedAuthority;

@Data
@Entity
public class Authority implements GrantedAuthority {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="authority_id")
	private Long id;
	private String authority;
}
