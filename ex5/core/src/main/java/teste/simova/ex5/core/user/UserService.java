package teste.simova.ex5.core.user;

import teste.simova.ex5.service.CRUDService;

public interface UserService extends CRUDService<User, Long> {

	public abstract void redefinePassword(Long id, String password);

}
