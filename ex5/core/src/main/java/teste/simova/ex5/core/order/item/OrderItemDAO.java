package teste.simova.ex5.core.order.item;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import teste.simova.ex5.service.CRUDRepository;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/4/13
 * Time: 9:32 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
interface OrderItemDAO extends CRUDRepository<OrderItem, Long> {
    @Query("select oi from OrderItem oi where(oi.name like ?1)")
    Page<OrderItem> searchAll(String q, Pageable pageable);
}
