package teste.simova.ex5.core.client;

import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/4/13
 * Time: 9:15 PM
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Data
@Table(name="clients")
@JsonIgnoreProperties(value = {"text"})
public class Client {
    @Id
    @GeneratedValue
    @Column(name="client_id")
    private Long id;

    private String name;
    private String email;
    private String tel;

}
