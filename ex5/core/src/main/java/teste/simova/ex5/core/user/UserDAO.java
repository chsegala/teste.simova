package teste.simova.ex5.core.user;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import teste.simova.ex5.service.CRUDRepository;


@Repository
public interface UserDAO extends CRUDRepository<User, Long> {
	User findOneByEmail(String username);
	
	@Query("select u from User u where (u.name like ?1 or u.email like ?1)")
	Page<User> searchAll(String q, Pageable pageable);
}
