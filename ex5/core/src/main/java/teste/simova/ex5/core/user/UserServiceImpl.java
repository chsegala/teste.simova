package teste.simova.ex5.core.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import teste.simova.ex5.service.CRUDServiceImpl;

@Component(value = "userService")
class UserServiceImpl extends CRUDServiceImpl<User, Long> implements
		UserService, UserDetailsService {
	@Autowired
	@Qualifier("passwordEncoder")
	private PasswordEncoder encoder;

	@Autowired
	public UserServiceImpl(UserDAO dao) {
		super(dao);
	}

	@Override
	public <S extends User> S save(S entity) {
		User p = null;
		if(entity.getId() != null){
			p = this.findOne(entity.getId());
		}
		
		if(entity.getId() == null || (p != null && !p.getPassword().equals(entity.getPassword()))){
			String encoded = encoder.encode(entity.getPassword());
			entity.setPassword(encoded);
		}

		return super.save(entity);
	}
	
	public void redefinePassword(Long id, String password){
		User user = findOne(id);
		String encoded = encoder.encode(password);
		user.setPassword(encoded);
		
		save(user);
	}

	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		User user = ((UserDAO) dao).findOneByEmail(username);
		if (user == null)
			throw new UsernameNotFoundException("{user_not_found}");
		return user;
	}

}
