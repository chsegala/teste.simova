package teste.simova.ex5.core.order.item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import teste.simova.ex5.service.CRUDRepository;
import teste.simova.ex5.service.CRUDServiceImpl;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/4/13
 * Time: 9:34 PM
 * To change this template use File | Settings | File Templates.
 */

@Component
class OrderItemServiceImpl extends CRUDServiceImpl<OrderItem, Long> implements OrderItemService {
    @Autowired
    public OrderItemServiceImpl(OrderItemDAO dao) {
        super(dao);
    }
}
