package teste.simova.ex5.core.config;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath*:META-INF/spring/testContext.xml")
public class SpringBaseTestClass extends AbstractTransactionalJUnit4SpringContextTests {

	public static class TestConfig{
		
	}
}
