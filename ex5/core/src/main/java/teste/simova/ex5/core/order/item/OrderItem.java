package teste.simova.ex5.core.order.item;

import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import teste.simova.ex5.core.order.Order;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/4/13
 * Time: 9:29 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="order_items")
@Data
@JsonIgnoreProperties(value = {"order"})
public class OrderItem {
    @Id
    @GeneratedValue
    @Column(name = "order_item_id")
    private Long id;

    @Column(name = "order_id")
    private Long orderId;

    private String name;
    private BigDecimal price;
    private int amount;

}
