package teste.simova.ex5.core.client;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import teste.simova.ex5.service.CRUDRepository;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/4/13
 * Time: 9:18 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
interface ClientDAO extends CRUDRepository<Client, Long> {
    @Query("select c from Client c where (c.name like ?1 or c.email like ?1)")
    Page<Client> searchAll(String q, Pageable pageable);
}
