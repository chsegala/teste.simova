package teste.simova.ex5.core.user;

import teste.simova.ex5.service.CRUDService;

public interface AuthorityService extends CRUDService<Authority, Long> {

}
