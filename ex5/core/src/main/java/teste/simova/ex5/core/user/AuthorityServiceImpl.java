package teste.simova.ex5.core.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import teste.simova.ex5.service.CRUDServiceImpl;

@Component
class AuthorityServiceImpl extends CRUDServiceImpl<Authority, Long> implements AuthorityService {

	@Autowired
	public AuthorityServiceImpl(AuthorityDAO dao) {
		super(dao);
	}

}
