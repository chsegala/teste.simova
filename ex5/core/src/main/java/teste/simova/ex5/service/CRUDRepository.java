package teste.simova.ex5.service;

import java.io.Serializable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CRUDRepository<T, ID extends Serializable> extends JpaRepository<T, ID> {
	@Query
	public abstract Page<T> searchAll(String q, Pageable pageable);
}
