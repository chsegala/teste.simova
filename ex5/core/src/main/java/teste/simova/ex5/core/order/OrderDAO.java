package teste.simova.ex5.core.order;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import teste.simova.ex5.service.CRUDRepository;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/4/13
 * Time: 9:25 PM
 * To change this template use File | Settings | File Templates.
 */

@Repository
interface OrderDAO extends CRUDRepository<Order, Long> {
    @Query("select o from Order o where (o.client.name like ?1)")
    Page<Order> searchAll(String q, Pageable pageable);
}
