package teste.simova.ex5.core.user;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import teste.simova.ex5.service.CRUDRepository;

public interface AuthorityDAO extends CRUDRepository<Authority, Long> {
	@Query("select a from Authority a")
	public Page<Authority> searchAll(String q, Pageable pageable);
}
