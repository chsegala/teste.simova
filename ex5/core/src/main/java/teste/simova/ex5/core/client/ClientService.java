package teste.simova.ex5.core.client;

import teste.simova.ex5.service.CRUDService;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/4/13
 * Time: 9:20 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ClientService extends CRUDService<Client, Long> {
}
