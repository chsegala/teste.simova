package teste.simova.ex5.core.order;

import lombok.Data;
import org.codehaus.jackson.annotate.JsonProperty;
import teste.simova.ex5.core.client.Client;
import teste.simova.ex5.core.order.item.OrderItem;

import javax.persistence.*;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/4/13
 * Time: 9:22 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Data
@Table(name="orders")
public class Order {
    @Id
    @GeneratedValue
    @Column(name="order_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "order_id")
    private Set<OrderItem> items = new HashSet<OrderItem>();

    private Date createdAt;

    @PrePersist
    public void prePersist(){
        this.createdAt = new Date();
    }
}
