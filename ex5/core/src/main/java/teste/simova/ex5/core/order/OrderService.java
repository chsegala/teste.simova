package teste.simova.ex5.core.order;

import teste.simova.ex5.service.CRUDService;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/4/13
 * Time: 9:26 PM
 * To change this template use File | Settings | File Templates.
 */
public interface OrderService extends CRUDService<Order, Long> {
}
