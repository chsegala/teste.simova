package teste.simova.ex5.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CRUDService<T, ID extends Serializable> {
	T findOne(ID id);
	
	List<T> findAll();
	Page<T> findAll(Pageable pageable);
	Page<T> searchAll(String q, Pageable pageable);
	
	<S extends T> S save(S entity);
	
	void delete(ID id);
	void delete(T entity);
	
	long count();
}
