package teste.simova.ex5.core.user;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import teste.simova.ex5.core.config.SpringBaseTestClass;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class UserServiceTest extends SpringBaseTestClass {
	@Autowired
	private UserService service;
	@Autowired
	private StandardPasswordEncoder encoder;
	
	@Autowired
	private UserService as;
	
	@Test
	public void findAllTest(){
		List<User> users = service.findAll();
		assertEquals(1, users.size());
	}

    @Test
    public void generatePassword(){
        String password = encoder.encode("admin");
    }
}
