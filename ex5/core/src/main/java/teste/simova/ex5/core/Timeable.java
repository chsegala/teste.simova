package teste.simova.ex5.core;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public interface Timeable {
	@Column(name = "created_at")
	@Temporal(TemporalType.TIMESTAMP)
	public abstract Date getCreatedAt();
	
	@Column(name = "updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	public abstract Date getUpdatedAt();
}
