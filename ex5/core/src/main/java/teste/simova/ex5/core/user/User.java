package teste.simova.ex5.core.user;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Data
@Entity
@Table(uniqueConstraints=@UniqueConstraint(name="email", columnNames="email"))
@JsonIgnoreProperties(ignoreUnknown = true) 
public class User implements UserDetails{
	@Id
	@Column(name="user_id")
	@GeneratedValue
	private Long id;
	
	@NotEmpty
	@Email
	@Size(max=300, min=5, message="{email.length}")
	private String email;
	
	@NotEmpty
	private String password;
	
	@NotEmpty
	@Size(max=300, min=5, message="{name.length}")
	private String name;
	
	@NotNull
	@Getter(value=AccessLevel.PRIVATE)
	@Setter
    @Column(columnDefinition = "BIT", length = 1)
	private Boolean enabled;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_at", updatable= false)
	private Date createdAt;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at")
	private Date updatedAt;
	
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="user_authority",
		joinColumns = @JoinColumn(name="user_id"),
		inverseJoinColumns = @JoinColumn(name="authority_id")
	)
	private Collection<Authority> authorities = new ArrayList<Authority>(0);
	
	@PrePersist
	private void prePersist(){
		this.createdAt = this.updatedAt = new Date();
	}
	
	@PreUpdate
	private void preUpdate(){
		this.updatedAt = new Date();
	}

	public String getUsername() {
		return this.getEmail();
	}

	public boolean isAccountNonExpired() {
		return true;
	}

	public boolean isAccountNonLocked() {
		return true;
	}

	public boolean isCredentialsNonExpired() {
		return true;
	}

	public boolean isEnabled() {
		return this.enabled == null ? false : this.enabled;
	}
}
