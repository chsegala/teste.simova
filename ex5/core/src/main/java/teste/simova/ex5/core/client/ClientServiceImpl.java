package teste.simova.ex5.core.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import teste.simova.ex5.service.CRUDRepository;
import teste.simova.ex5.service.CRUDServiceImpl;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/4/13
 * Time: 9:21 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class ClientServiceImpl extends CRUDServiceImpl<Client, Long> implements ClientService {

    @Autowired
    public ClientServiceImpl(ClientDAO dao) {
        super(dao);
    }
}
