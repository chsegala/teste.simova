package teste.simova.ex5.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public abstract class CRUDServiceImpl<T, ID extends Serializable> implements CRUDService<T, ID> {
	
	protected CRUDRepository<T, ID> dao;

	@Autowired
	public CRUDServiceImpl(CRUDRepository<T, ID> dao){
		this.dao = dao;
	}

	public <S extends T> S save(S entity) {
		return dao.save(entity);
	}

	public List<T> findAll() {
		return dao.findAll();
	}

	public Page<T> findAll(Pageable pageable) {
		return dao.findAll(pageable);
	}
	
	public Page<T> searchAll(String q, Pageable pageable) {
		return dao.searchAll(q, pageable);
	}

	public T findOne(ID id) {
		return dao.findOne(id);
	}

	public long count() {
		return dao.count();
	}

	public void delete(ID id) {
		dao.delete(id);
	}

	public void delete(T entity) {
		dao.delete(entity);
	}
}
