<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="utils" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><decorator:title></decorator:title></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<utils:css url="/resources/bootstrap/css/bootstrap.css" media="screen"/>
<utils:css url="/resources/bootstrap/css/bootstrap-responsive.css" media="screen"/>
</head>
<body>
	<utils:script url="/resources/js/jquery-1.10.1.js"/>
	<utils:script url="/resources/bootstrap/js/bootstrap.js"/>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12">
				<div class="navbar">
					<div class="navbar-inner">
						<utils:a href="/" cssClass="brand">Bookjet</utils:a>
					</div>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<decorator:body />
			</div>
		</div>
	</div>
</body>
</html>