<%@ page contentType="text/html; UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="utils"%>

<utils:script url="/resources/js/controller/users-controller.js"></utils:script>
<div class="">
    <div class="alert hide">

    </div>
    <form name="form" class="form" action="/admin/user/" method="POST">
        <fieldset>
            <legend>{{'Usuário' | i18n}}</legend>
            <div class="container-fluid">
                <div class="span8">
                    <field-decorator label="Nome">
                        <input type="text" ng-model="user.name" name="name" required/>
                    </field-decorator>
                    <field-decorator label="Email">
                        <input type="email" ng-model="user.email" name="email" required/>
                    </field-decorator>
                    <div ng-show="!isEdit">
                        <field-decorator label="Senha">
                            <input type="text" ng-model="user.password" name="password" ng-required="!isEdit"/>
                        </field-decorator>
                        <field-decorator label="Confirmar Senha">
                            <input type="text" ng-model="confirmPassword" name="confirmPassword" ng-required="!isEdit"/>
                        </field-decorator>
                    </div>
                </div>

                <div class="span4">
                    <fieldset>
                        <legend>{{'Permissões' | i18n}}</legend>
                        <div class="well">
                            <div class="control-group">
                                <input type="text" ui-select2="select2Taxi" ng-model="user.taxiId" data-placeholder="{{'Taxi' | i18n}}" style="width: 250px" />
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    <input type="checkbox" name="enabled" ng-model="user.enabled">
                                    {{'Habilitado' | i18n}}
                                </label>
                            </div>
                            <div class="control-group">
                                <label class="control-label" ng-repeat="a in authorityList">
                                    <input type="checkbox" name="authority" ng-click="authority(a)" ng-checked="hasAuthority(a)")> {{a.authority | i18n}}
                                </label>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="button" class="btn btn-primary" ng-disabled="form.$invalid" ng-click="save()">
                        <i class="icon-ok icon-white"></i> {{'Salvar' | i18n}}
                    </button>
                    <utils:a href="/users" cssClass="btn btn-danger">
                        <i class="icon-arrow-left icon-white"></i> {{'Voltar' | i18n}}
                    </utils:a>
                </div>
            </div>
        </fieldset>
    </form>
</div>