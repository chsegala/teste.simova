

<%@ taglib tagdir="/WEB-INF/tags" prefix="utils"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<utils:script url="/resources/js/controller/clients-controller.js"></utils:script>
<div class="">
	<div class="form-inline">
		<div class="input-prepend">
			<span class="add-on"><i class="icon-search"></i></span><input type="text" ng-model="q" />
		</div>
		<button class="btn" ng-click="search()" type="button">{{'Buscar' | i18n}}</button>
	</div>
	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr i18n-find="th">
				<th sort-column="'name'" paging-and-sorting="pagingAndSorting">Nome</th>
				<th sort-column="'email'" paging-and-sorting="pagingAndSorting">Email</th>
                <th sort-column="'tel'" paging-and-sorting="pagingAndSorting">Telefone</th>
				<th width="20%"></th>
			</tr>
		</thead>
		<tbody>
			<tr data-ng-repeat="client in clientList">
				<td data-ng-bind="client.name"></td>
				<td data-ng-bind="client.email"></td>
                <td data-ng-bind="client.tel"></td>
				<td>
					<utils:a href="/clients/{{client.id}}/edit" cssClass="btn btn-primary">
						<i class="icon-edit icon-white" data-toggle="tooltip"
							title="{{'Editar' | i18n}}"></i>
					</utils:a>
					<button type="button" ng-click="destroy(client)"
						class="btn btn-danger">
						<i class="icon-trash icon-white" data-toggle="tooltip"
							title="{{'Excluir' | i18n}}"></i>
					</button>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<tr>
					<td colspan="3">
						<pages paging-and-sorting="pagingAndSorting"></pages>
					</td>
					<td>
						<page-size paging-and-sorting="pagingAndSorting"></page-size>
						{{'Itens por página' | i18n}}
					</td>
				</tr>
			</tr>
		</tfoot>
	</table>
	<utils:a href="/clients/new" cssClass="btn btn-primary">
		<i class="icon-plus icon-white"></i> {{'Novo' | i18n}}
	</utils:a>
</div>
