<%@ page contentType="text/html; UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="utils" %>

<utils:script url="/resources/js/controller/clients-controller.js"></utils:script>
<div class="">
    <div class="alert hide">

    </div>
    <form name="form" class="form" action="/admin/user/" method="POST">
        <fieldset>
            <legend>{{'Usuário' | i18n}}</legend>
            <field-decorator label="Nome">
                <input type="text" ng-model="client.name" name="name" required/>
            </field-decorator>
            <field-decorator label="Email">
                <input type="email" ng-model="client.email" name="email" required/>
            </field-decorator>
            <field-decorator label="Telefone">
                <input type="text" ng-model="client.tel" name="tel"/>
            </field-decorator>

            <div class="control-group">
                <div class="controls">
                    <button type="button" class="btn btn-primary" ng-disabled="form.$invalid" ng-click="save()">
                        <i class="icon-ok icon-white"></i> {{'Salvar' | i18n}}
                    </button>
                    <utils:a href="/clients" cssClass="btn btn-danger">
                        <i class="icon-arrow-left icon-white"></i> {{'Voltar' | i18n}}
                    </utils:a>
                </div>
            </div>
        </fieldset>
    </form>
</div>