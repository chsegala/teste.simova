<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Login - Bookjet</title>
</head>
<body>
	<div class="span6 well">
		Bem Vindo!
	</div>
	<div class="span6 well">
		<form class="form-horizontal" action="j_spring_security_check" method="post">
			<fieldset>
				<legend>Login</legend>
				<c:if test="${ not empty error }">
					<div class="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						${ error }
					</div>
				</c:if>
				<div class="control-group">
					<label class="control-label" for="inputEmail">
						Email
					</label>
					<div class="controls">
						<input type="email" id="inputEmail" name="j_username" placeholder="Email" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputPassword">
						Senha
					</label>
					<div class="controls">
						<input type="password" id="inputPassword" name="j_password" placeholder="Senha" />
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<label>
							<input type="checkbox" id="rememberMe" name="_spring_security_remember_me" checked="checked" /> 
							Manter-me conectado
						</label>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-primary btn-large">
							Entrar
						</button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</body>
</html>