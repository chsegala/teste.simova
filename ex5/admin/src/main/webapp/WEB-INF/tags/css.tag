<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ attribute name="url" description="URL absoluta do arquivo a ser importado" required="true" type="java.lang.String" %>
<%@ attribute name="media" description="Media html attribute" required="false" type="java.lang.String" %>
<%@ attribute name="id" description="ID" required="false" type="java.lang.String" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${not fn:startsWith(url,'/resources') }">
		<c:url value="${url}" var="in_url" ></c:url>	
</c:if>
<c:if test="${empty in_url}">
	<c:if test="${not empty applicationVersion}">
		<c:set var="versionAdd" value="/resources-${applicationVersion}"></c:set>
		<c:url value="${fn:replace(url,'/resources',versionAdd)}" var="in_url" ></c:url>
	</c:if>
	<c:if test="${empty applicationVersion}">
		<c:url value="${url}" var="in_url" ></c:url>
	</c:if>
</c:if>

<c:if test="${empty stylesheetMap or empty stylesheetMap[in_url]}">
	<c:if test="${empty stylesheetMap }">
		<jsp:useBean id="stylesheetMap" class="java.util.HashMap" scope="request"/> 
	</c:if>
	<c:set target="${stylesheetMap}" property="${in_url}" value="${true}"></c:set>
<link href="${in_url}" rel="stylesheet" media="${media}" id="${id}"/>
</c:if>