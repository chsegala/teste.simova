<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<form class="login-form" action="j_spring_security_check" method="post">
		<fieldset style="margin-bottom: -20px;">
			<h1>Login</h1>
			<br>
			<p>
			<input id="j_username" name="j_username" size="20" maxlength="50" type="text" style="height: 40px; width: 350px;" placeholder="<spring:message code="application.username"/>"/>
			</p>

			<p>
			<input id="j_password" name="j_password" size="20" maxlength="50" type="password" style="height: 40px; width: 350px;" placeholder="<spring:message code="application.password"/>"/>
			</p>
			
			<table>
				<tr>
					<td style="padding-bottom: 0px;" >
						<input type="checkbox" name="_spring_security_remember_me" id="keepConnected">
					</td>
					<td style="padding-left: 5px">
						<label for="keepConnected"><spring:message code="application.remember"/></label>
					</td>
					<td align="right" style="padding-left: 30px; padding-top: 10px">
						<p><input class="btn-primary btn-large" type="submit" value="Entrar" style="float: right; margin-left: 47px;"/></p>
					</td>
				</tr>
			</table>
			<p class="error" style="color: red;">${error}</p>
		</fieldset>
	</form>
