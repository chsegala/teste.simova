var translations={
	'pt':{
		//application
		'Novo': 'Novo',
		'Salvar': 'Salvar',
		'Voltar': 'Voltar',
		'Você deseja realmente apagar o registro?': 'Você deseja realmente apagar o registro?',
		
		//user
		'Nome': 'Nome',
		'Senha': 'Senha',
		'Email': 'Email',
		'Confirmar Senha': 'Confirmar Senha',
		'Habilitado': 'Habilitado',
		'A confirmação não corresponde à senha': 'A confirmação não corresponde à senha',
	}
};