angular.module('appResource', ['ng', 'ngResource']).factory('appResource', 
			['$resource', '$route', function($resource, $route){
	var defaultsDefault = {};
	var actionsDefault = {
			'query': {method: 'GET', isArray: false},
			'update': {method: 'PUT'},
			'destroy': {method: 'DELETE'}
		};
	
	function AppResourceProvider(url, defaults, actions){
		actions = $.extend({}, actionsDefault, actions);
		defaults = $.extend({}, defaultsDefault, defaults);
		
		var res = $resource(url, defaults, actions);
		return res;
	};
	
	return AppResourceProvider;
}]);