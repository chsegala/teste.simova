var module = angular.module('defaultInputs', ['ng']);

module.directive('fieldDecorator', function(){
	return{
		restrict: 'E',
		replace: true,
		transclude: true,
		template: '<div class="control-group"><label class="control-label" for="input{{name}}">{{label | i18n}}</label><div class="controls"></div></div>',
		scope: {
			label: '@',
		},
		compile: function(tEle, tAttrs, transclude){
			return function(scope, iEle, iAttrs, ctrl){
			transclude(scope.$parent, function(clone){
				$.each(clone, function(index, item){
					if(!$(item).is('input'))return;
					scope.name = $(item).attr('name');
					$(item).attr('id', 'input' + scope.name);
				});
				iEle.find('div.controls').append(clone);
			});
			
			};
		}
	};
});