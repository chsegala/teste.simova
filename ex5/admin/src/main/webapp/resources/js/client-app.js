var ClientApp = angular.module('ClientApp', ['ui.utils', 'ui.select2', 'defaultInputs', 'currencyInput', 'datetimePicker', 'appResource', 'bootstrap', 'i18n', 'pagingAndSorting', 'models']);

ClientApp.config(function ($locationProvider, $routeProvider) {
    // $locationProvider.hashPrefix(!);
    $locationProvider.html5Mode(true);

    $routeProvider.when('/', {
        redirectTo: '/orders'
    })
        //users
        .when('/users/', {
            controller: 'UserIndexCtrl',
            templateUrl: base_url + 'templates/users/index'
        }).when('/users/new/', {
            controller: 'UserCreateCtrl',
            templateUrl: base_url + 'templates/users/form'
        }).when('/users/:id/edit', {
            controller: 'UserEditCtrl',
            templateUrl: base_url + 'templates/users/form'
        }).when('/users/:id/redefine', {
            controller: 'UserRedefineCtrl',
            templateUrl: base_url + 'templates/users/redefine'
        }).when('/users/change_password', {
            controller: 'UserChangePasswordCtrl',
            templateUrl: base_url + 'templates/users/change_password'
        })

        //clients
        .when('/clients', {
            controller: 'ClientsIndexCtrl',
            templateUrl: base_url + 'templates/clients/index'
        })
        .when('/clients/new', {
            controller: 'ClientsCreateCtrl',
            templateUrl: base_url + 'templates/clients/form'
        })
        .when('/clients/:id/edit', {
            controller: 'ClientsEditCtrl',
            templateUrl: base_url + 'templates/clients/form'
        })

        //orders
        .when('/orders', {
            controller: 'OrderIndexCtrl',
            templateUrl: base_url + 'templates/orders/index'
        })
        .when('/orders/new', {
            controller: 'OrderCreateCtrl',
            templateUrl: base_url + 'templates/orders/form'
        })
        .when('/orders/:id/edit', {
            controller: 'OrderEditCtrl',
            templateUrl: base_url + 'templates/orders/form'
        })

        .otherwise({
            redirectTo: '/'
        });
});

// DIRECTIVES //

