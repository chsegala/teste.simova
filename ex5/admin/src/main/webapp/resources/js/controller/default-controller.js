var DefaultIndexCtrl = function(model, attr, $scope, $pagingAndSorting, $location, $routeParams, $locale, $filter){
	$scope[attr+'List'] = [];
	$scope.q = '';
	$scope.pagingAndSorting = $pagingAndSorting({
		url: context_path + $location.path(),
		property: attr + 'List',
		success: function(data, page){
			$scope[attr+'List'] = data[attr+'List'].content;
			$scope.page = page;
		}
	});
	
	$scope.pagingAndSorting.query();
	$scope.search = function(){
		$scope.pagingAndSorting.query({q: $scope.q});
	};
	
	$scope.destroy = function(item){
		if (!confirm($filter('i18n')('Você deseja realmente apagar o registro?')))
			return;
		
			model.destroy($.extend({}, $routeParams, {id: item.id}), function(){
			angular.forEach($scope[attr+'List'], function(it, i){
				if(it.id === item.id){
					$scope[attr+'List'].splice(i,1);
					return;
				}
			});
			flash.success($filter('i18n')('Registro excluído com sucesso'));
		}, function(){
			flash.error($filter('i18n')('Falha ao excluir o registro'));
		});
	};
};

var DefaultCreateCtrl = function(model, attr, editUrl, $scope, $location, $routeParams, $locale, $filter){
	$scope[attr] = {};
	model.get($.extend({}, $routeParams, {id: 'new'}), function(data){
		$scope[attr] = new model(data[attr]);
	});
	
	$scope.save = function(){
		var params = $routeParams;
		delete(params.id);
		model.save(params, $scope[attr],
		function(data){
			flash.success($filter('i18n')('Registro salvo.'));
			$location.path(editUrl + '/' + data[attr].id + '/edit');
		}, function(data){
			var msg = $filter('i18n')('Falha ao salvar o registro.')+"<br> ";
			angular.forEach(data.data,function(item){
				msg+=$filter('i18n')(item.message)+"<br>";
			});
			flash.error(msg,0);;
		});
	};
};

var DefaultEditCtrl = function(model, attr, $scope, $location, $routeParams, $locale, $filter){
	$scope[attr] = {};
	model.get($routeParams, function(data){
		$scope[attr] = new model(data[attr]);
	});
	
	$scope.save = function(){
		var params = $routeParams;
		delete(params.id);
		model.update(params, $scope[attr],
		function(data){
			flash.success($filter('i18n')('Registro salvo.'));
		}, function(data){
			var msg = $filter('i18n')('Falha ao salvar o registro.')+"<br> ";
			angular.forEach(data.data,function(item){
				msg+=$filter('i18n')(item.message)+"<br>";
			});
			flash.error(msg,0);;
		});
	};
};