var flash = {
	alert : function(message, type,time) {
		$('#alert span').html(message);
		$('#alert a.close').click(function() {
			$('#alert').hide();
		});

		var el = $('#alert').removeClass('alert-success alert-error').addClass(type)
				.slideDown();
		if(time===0)
			el.click(function(e){
				el.delay(500).slideUp();
				el.off('click');
			});
		else if(time)
			el.delay(time).slideUp();
		else 
			el.delay(3000).slideUp();
		
	},
	success : function(message,time) {
		this.alert(message, 'alert-success',time);
	},
	error : function(message,time) {
		this.alert(message, 'alert-error',time);
	}
};