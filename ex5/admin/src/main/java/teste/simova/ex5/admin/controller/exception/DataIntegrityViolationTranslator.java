package teste.simova.ex5.admin.controller.exception;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@Slf4j
public class DataIntegrityViolationTranslator {
	
	
	@Autowired
	private MessageSource messageSource;
	public List<ExceptionDTO> translate(DataIntegrityViolationException exception,Locale locale ){
		String constraint = getConstraintName(exception);
		if(constraint!=null){
			String message = messageSource.getMessage(constraint, new Object[0],constraint, locale);
			String path = messageSource.getMessage(constraint+"_field", new Object[0],constraint+"_field", locale);
			return Arrays.asList(new ExceptionDTO(path,message));
		}
		return Arrays.asList();
	}
	
	private static Pattern NAME_PATTERN = Pattern.compile("'(.*?)'");
	private String getConstraintName(DataIntegrityViolationException exception) {
		Throwable rootCause =  exception.getRootCause();
		if(rootCause instanceof MySQLIntegrityConstraintViolationException){
			MySQLIntegrityConstraintViolationException mException = (MySQLIntegrityConstraintViolationException) rootCause;
			String message = mException.getMessage();
			Matcher matcher = NAME_PATTERN.matcher(message);
			if(matcher.find() && matcher.find()){
				return matcher.group(1);
			}
		}
		throw exception;
	}
}
