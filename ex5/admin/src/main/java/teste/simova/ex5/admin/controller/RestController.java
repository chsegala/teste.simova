package teste.simova.ex5.admin.controller;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import teste.simova.ex5.service.CRUDService;

@SuppressWarnings("rawtypes")
public interface RestController<TService extends CRUDService, TEntity, ID extends Serializable> {

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public abstract ModelAndView delete(@PathVariable ID id);
	
	@RequestMapping(value = "", method = { RequestMethod.POST, RequestMethod.PUT }, produces = "application/json")
	public abstract ModelAndView save(@RequestBody TEntity entity, HttpServletResponse response, WebRequest request);

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public abstract ModelAndView getNew(HttpServletRequest request)throws ReflectiveOperationException;

	@RequestMapping(value = { "/{id}", "/{id}/edit" }, method = RequestMethod.GET)
	public abstract ModelAndView getOne(@PathVariable ID id);

	@RequestMapping(value = "", method = RequestMethod.GET)
	public abstract ModelAndView index(HttpServletRequest request);

}
