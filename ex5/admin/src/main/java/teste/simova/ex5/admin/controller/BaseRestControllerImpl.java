package teste.simova.ex5.admin.controller;

import lombok.Getter;
import lombok.Setter;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import teste.simova.ex5.admin.controller.exception.DataIntegrityViolationTranslator;
import teste.simova.ex5.admin.controller.exception.ExceptionDTO;
import teste.simova.ex5.service.CRUDService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@SuppressWarnings("rawtypes")
public abstract class BaseRestControllerImpl<TService extends CRUDService, TEntity, ID extends Serializable> implements RestController<TService, TEntity, ID> {

    @Getter
    @Setter
    private TService service;

    @Getter
    @Setter
    private String propName;

    @Getter
    @Setter
    private Class<TEntity> entityClass;

    @Autowired
    private DataIntegrityViolationTranslator exceptionTranslator;

    public BaseRestControllerImpl(TService service, String prop, Class<TEntity> clazz) {
        this.service = service;
        this.propName = prop;
        this.entityClass = clazz;
    }

    public Pageable getPageable(HttpServletRequest request) {
        Pageable pageable = null;
        if (!StringUtils.isBlank(request.getParameter("page"))) {
            Integer page = Integer.valueOf(request.getParameter("page"));
            Integer size = Integer.valueOf(request.getParameter("size"));
            if (!StringUtils.isBlank(request.getParameter("sort"))) {
                Direction dir = Direction.ASC;
                if ("desc".equals(request.getParameter("dir")))
                    dir = Direction.DESC;

                Sort sort = new Sort(dir, request.getParameter("sort"));
                pageable = new PageRequest(page, size, sort);
            } else {
                pageable = new PageRequest(page, size);
            }
        }
        return pageable;
    }

    public String getQuery(HttpServletRequest request) {
        String q = "";
        if (!StringUtils.isBlank(request.getParameter("q"))) {
            q = request.getParameter("q");
        }
        return "%" + q + "%";
    }

    public ModelAndView index(HttpServletRequest request) {
        Pageable pageable = getPageable(request);
        String q = getQuery(request);
        return new ModelAndView("blank", getPropName() + "List", service.searchAll(q, pageable));
    }

    @SuppressWarnings("unchecked")
    public ModelAndView getOne(@PathVariable ID id) {
        return new ModelAndView("blank", getPropName(), service.findOne(id));
    }

    public ModelAndView getNew(HttpServletRequest request) throws ReflectiveOperationException {
        TEntity newInstance = getEntityClass().newInstance();
        return new ModelAndView("blank", getPropName(), newInstance);
    }

    @SuppressWarnings("unchecked")
    public ModelAndView save(@RequestBody TEntity entity, HttpServletResponse response, WebRequest request) {
        return new ModelAndView("blank", getPropName(), service.save(entity));
    }


    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseBody
    public String dbConstraintHandler(DataIntegrityViolationException exception, HttpServletResponse resp, Locale locale) throws JsonGenerationException, JsonMappingException, IOException {
        resp.setStatus(500);
        resp.setCharacterEncoding("UTF-8");
        List<ExceptionDTO> result = exceptionTranslator.translate(exception, locale);
        return new ObjectMapper().writeValueAsString(result);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public String constraintHandler(ConstraintViolationException exception, HttpServletResponse resp) throws JsonGenerationException, JsonMappingException, IOException {
        resp.setStatus(500);
        resp.setCharacterEncoding("UTF-8");
        List<ExceptionDTO> result = new ArrayList<ExceptionDTO>();
        for (val violation : exception.getConstraintViolations()) {
            result.add(new ExceptionDTO(violation.getPropertyPath().toString(), violation.getMessage()));
        }
        return new ObjectMapper().writeValueAsString(result);
    }

    @SuppressWarnings("unchecked")
    public ModelAndView delete(@PathVariable ID id) {
        service.delete(id);
        return new ModelAndView("blank", getPropName(), null);
    }
}
