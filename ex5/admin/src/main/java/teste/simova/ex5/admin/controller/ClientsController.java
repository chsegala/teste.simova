package teste.simova.ex5.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import teste.simova.ex5.core.client.Client;
import teste.simova.ex5.core.client.ClientService;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/4/13
 * Time: 9:55 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/clients")
public class ClientsController extends BaseRestControllerImpl<ClientService, Client, Long> {
    @Autowired
    public ClientsController(ClientService service) {
        super(service, "client", Client.class);
    }
}
