package teste.simova.ex5.admin.controller;

import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;

@Controller
@RequestMapping("templates")
public class TemplatesController {
	@Autowired
	@Qualifier("templateViewResolver")
	private ViewResolver viewResolver;
	
	@RequestMapping("/**")
	public void templateResolver(Map<String,?> map, HttpServletRequest request, HttpServletResponse response, Locale locale) throws Exception{
		Matcher matcher = Pattern.compile("templates/.*").matcher(request.getRequestURL());
		matcher.find();
		String path = matcher.group();
		
		View view = viewResolver.resolveViewName(path, locale);
		view.render(map, request, response);
	}
}
