package teste.simova.ex5.admin.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import teste.simova.ex5.core.user.Authority;
import teste.simova.ex5.core.user.AuthorityService;

@Controller
@RequestMapping("/authorities")
public class AuthorityController extends
		BaseRestControllerImpl<AuthorityService, Authority, Long> {

	@Autowired
	public AuthorityController(AuthorityService service) {
		super(service, "authority", Authority.class);
	}

	@Override
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request) {
		return new ModelAndView("blank", "authorityList", getService()
				.findAll());
	}

}
