package teste.simova.ex5.admin.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import teste.simova.ex5.core.user.User;
import teste.simova.ex5.core.user.UserService;

@Controller
@RequestMapping("/users")
public class UserController extends BaseRestControllerImpl<UserService, User, Long> {
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ChangePasswordDTO {
		private String oldPassword;
		private String password;
	}
	
	@Autowired
	private PasswordEncoder encoder;

	@Autowired
	public UserController(UserService service) {
		super(service, "user", User.class);
	}
	
	@RequestMapping(value = "/{id}/redefine", method=RequestMethod.GET)
	public ModelAndView getRedefine(@PathVariable Long id){
		return new ModelAndView("blank", "", "");
	}
	@RequestMapping(value = "/{id}/redefine", method=RequestMethod.PUT)
	@ResponseBody
	public void putRedefine(@PathVariable Long id, @RequestBody String password){
		getService().redefinePassword(id, password);
	}
	@RequestMapping(value = "/change_password", method=RequestMethod.GET)
	public ModelAndView getChangePassword(){
		return new ModelAndView("blank", "", "");
	}
	@RequestMapping(value = "/change_password", method=RequestMethod.PUT)
	@ResponseBody
	public void putChangePassword(@RequestBody ChangePasswordDTO dto){
		Long principalId = ((User)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
		User user = getService().findOne(principalId);
		
		if(!encoder.matches(dto.getOldPassword(), user.getPassword()))
			throw new RuntimeException("Password doesn't match");
		
		getService().redefinePassword(user.getId(), dto.getPassword());
	}
}
