package teste.simova.ex5.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import teste.simova.ex5.core.order.item.OrderItem;
import teste.simova.ex5.core.order.item.OrderItemService;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/4/13
 * Time: 9:57 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/order-items")
public class OrderItemsController extends BaseRestControllerImpl<OrderItemService, OrderItem, Long> {
    @Autowired
    public OrderItemsController(OrderItemService service) {
        super(service, "orderItem", OrderItem.class);
    }
}
