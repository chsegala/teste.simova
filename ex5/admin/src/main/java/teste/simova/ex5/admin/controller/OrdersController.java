package teste.simova.ex5.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import teste.simova.ex5.core.order.Order;
import teste.simova.ex5.core.order.OrderService;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/4/13
 * Time: 9:55 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/orders")
public class OrdersController extends BaseRestControllerImpl<OrderService, Order, Long> {
    @Autowired
    public OrdersController(OrderService service) {
        super(service, "order", Order.class);
    }
}
