package teste.simova.ex5.admin.controller.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ExceptionDTO{
	private String path;
	private String message;
}
