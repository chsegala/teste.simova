package teste.simova.ex5.admin.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {
	@Autowired
	private MessageSource messageSource;
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String index(Principal principal){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return "home/index";
	}
	
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String login(@RequestParam( required = false) boolean error,
			Model model, HttpServletRequest request){
		
		if (error == true) {
			model.addAttribute("error", messageSource.getMessage("login.error", null, request.getLocale()));
		}
		
		return "login";
	}
}
