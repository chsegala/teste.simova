<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ attribute name="src" description="URL absoluta do arquivo a ser importado" required="true" type="java.lang.String" %>
<%@ attribute name="cssClass" description="Classe css da imagem" required="false" type="java.lang.String" %>
<%@ attribute name="style" description="Style da imagem" required="false" type="java.lang.String" %>
<%@ attribute name="alt" description="atributo da HTML" required="false" type="java.lang.String" %>
<%@ attribute name="onMouseOver" description="onMouseOver imagen" required="false" type="java.lang.String" %>
<%@ attribute name="onMouseOut" description="onMouseOut imagen" required="false" type="java.lang.String" %>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:url value="${src}" var="in_url" ></c:url>
<c:set value="" var="alt_att"></c:set>
<c:if test="${not empty alt }">
	<c:set value="alt='${alt}'" var="alt_att"></c:set>
</c:if>
<c:set value="" var="css_att"></c:set>
<c:if test="${not empty cssClass }">
	<c:set value="class='${cssClass}'" var="css_att"></c:set>
</c:if>
<c:set value="" var="style_att"></c:set>
<c:if test="${not empty style }">
	<c:set value="style='${style}'" var="style_att"></c:set>
</c:if>

<img src="${in_url}" ${css_att} ${style_att} ${alt_att} />
