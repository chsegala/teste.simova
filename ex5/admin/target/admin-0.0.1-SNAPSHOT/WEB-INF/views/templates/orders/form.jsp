<%@ page contentType="text/html; UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="utils" %>

<utils:script url="/resources/js/controller/orders-controller.js"></utils:script>
<div class="">
    <div class="alert hide">

    </div>
    <form name="form" class="form" action="/admin/user/" method="POST">
        <fieldset>
            <legend>{{'Pedidos' | i18n}}</legend>

            <field-decorator label="Cliente">
                <input type="text" ui-select2="select2Client" name="client" ng-model="order.client" style="width:250px" data-placeholder="Cliente" required/>
            </field-decorator>
            <table ng-show="isEdit">
                <thead>
                <tr>
                    <th>Item</th>
                    <th>Preço</th>
                    <th>Quantidade</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="item in order.items">
                    <td>{{item.name}}</td>
                    <td>{{item.price | currency}}</td>
                    <td>{{item.amount}}</td>
                    <td>
                        <button class="button" type="button" ng-click="removeItem(item);">Remover</button>
                    </td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td><input type="text" ng-model="item.name"/></td>
                    <td><input type="text" currency ng-model="item.price"/></td>
                    <td><input type="number" ng-model="item.amount"/></td>
                    <td>
                        <button class="button" type="button" ng-click="order.items.push(item); item = {orderId: order.id};">Adicionar
                        </button>
                    </td>
                </tr>
                </tfoot>
            </table>
        </fieldset>
    </form>
</div>
<div class="control-group">
    <div class="controls">
        <button type="button" class="btn btn-primary" ng-disabled="form.$invalid" ng-click="save()">
            <i class="icon-ok icon-white"></i> {{'Salvar' | i18n}}
        </button>
        <utils:a href="/orders" cssClass="btn btn-danger">
            <i class="icon-arrow-left icon-white"></i> {{'Voltar' | i18n}}
        </utils:a>
    </div>
</div>
</fieldset>
</form>
</div>