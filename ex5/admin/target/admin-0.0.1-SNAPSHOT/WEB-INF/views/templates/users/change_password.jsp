<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="utils"%>

<utils:script url="/resources/js/controller/users-controller.js"></utils:script>
<div class="">
	<form name="form" class="form" method="POST">
		<fieldset>
			<legend>{{'Redefinir Senha' | i18n}}</legend>
			<div class="control-group">
				<label class="control-label" for="inputName">
					{{'Senha antiga' | i18n}}
				</label>
				<div class="controls">
					<input type="password" name="oldPassword" id="inputOldPassoword" ng-model="oldPassword" required>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputName">
					{{'Nova Senha' | i18n}}
				</label>
				<div class="controls">
					<input type="password" name="password" id="inputPassword" ng-model="password" required>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputEmail">
					{{'Confirme nova senha' | i18n}}
				</label>
				<div class="controls">
					<input type="password" name="passwordConfirm" id="inputConfirm" ng-model="passwordConfirm" required>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<button type="button" class="btn btn-primary" ng-disabled="form.$invalid" ng-click="save()">
						<i class="icon-ok icon-white"></i> {{'Salvar' | i18n}}
					</button>
					<utils:a href="/users" cssClass="btn btn-danger">
						<i class="icon-arrow-left icon-white"></i> {{'Voltar' | i18n}}
					</utils:a>
				</div>
			</div>
		</fieldset>
	</form>
</div>