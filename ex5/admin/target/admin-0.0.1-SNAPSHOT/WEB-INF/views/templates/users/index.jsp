<%@ taglib tagdir="/WEB-INF/tags" prefix="utils"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<utils:script url="/resources/js/controller/users-controller.js"></utils:script>
<div class="">
	<div class="form-inline">
		<div class="input-prepend">
			<span class="add-on"><i class="icon-search"></i></span><input type="text" ng-model="q" />
		</div>
		<button class="btn" ng-click="search()" type="button">{{'Buscar' | i18n}}</button>
	</div>
	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr i18n-find="th">
				<th sort-column="'name'" paging-and-sorting="pagingAndSorting">Nome</th>
				<th sort-column="'email'" paging-and-sorting="pagingAndSorting">Email</th>
				<th sort-column="'enabled'" paging-and-sorting="pagingAndSorting">Habilitado</th>
				<th width="20%"></th>
			</tr>
		</thead>
		<tbody>
			<tr data-ng-repeat="user in userList">
				<td data-ng-bind="user.name"></td>
				<td data-ng-bind="user.email"></td>
				<td>
					<i class="icon-ok" ng-show="user.enabled"></i>
					<i class="icon-remove" ng-show="!user.enabled"></i>
				</td>
				<td>
					<sec:authorize access="hasAnyRole('taxi', 'admin')">
						<utils:a href="/users/{{user.id}}/aircrafts"
							cssClass="btn btn-primary">
							<i class="icon-plane icon-white" data-toggle="tooltip"
								title="{{'Aeronaves' | i18n}}"></i>
						</utils:a>
					</sec:authorize>
					<sec:authorize access="hasAnyRole('taxi', 'admin')">
						<utils:a href="/users/{{user.id}}/travels"
							cssClass="btn btn-primary">
							<i class="icon-briefcase icon-white" data-toggle="tooltip"
								title="{{'Viagens' | i18n}}"></i>
						</utils:a>
					</sec:authorize>
					<utils:a href="/users/{{user.id}}/edit" cssClass="btn btn-primary">
						<i class="icon-edit icon-white" data-toggle="tooltip"
							title="{{'Editar' | i18n}}"></i>
					</utils:a>
					<button type="button" ng-click="destroy(user)"
						class="btn btn-danger">
						<i class="icon-trash icon-white" data-toggle="tooltip"
							title="{{'Excluir' | i18n}}"></i>
					</button>
					<sec:authorize access="hasRole('admin')">
						<utils:a href="/users/{{user.id}}/redefine" cssClass="btn btn-success">
							<i class="icon-lock icon-white" data-toggle="tooltip"
								title="{{'Redefinir Senha' | i18n}}"></i>
						</utils:a>
					</sec:authorize>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<tr>
					<td colspan="3">
						<pages paging-and-sorting="pagingAndSorting"></pages>
					</td>
					<td>
						<page-size paging-and-sorting="pagingAndSorting"></page-size>
						{{'Itens por p�gina' | i18n}}
					</td>
				</tr>
			</tr>
		</tfoot>
	</table>
	<utils:a href="/users/new" cssClass="btn btn-primary">
		<i class="icon-plus icon-white"></i> {{'Novo' | i18n}}
	</utils:a>
</div>
