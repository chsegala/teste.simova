<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="utils" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<div class="well">
	<sec:authorize access="isAuthenticated()">
		<utils:a href="/users/">Gerenciar Usuários</utils:a>
	</sec:authorize>
</div>