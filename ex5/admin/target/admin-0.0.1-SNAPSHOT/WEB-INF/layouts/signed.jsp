<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="utils" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="ng-app:ClientApp"
      id="ng-app" ng-app="ClientApp" xmlns:ng="http://angularjs.org">
<head>
    <title><decorator:title></decorator:title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <utils:css url="/resources/bootstrap/css/bootstrap.css" media="screen"/>
    <utils:css url="/resources/bootstrap/css/bootstrap-responsive.css"
               media="screen"/>
    <utils:css url="/resources/select2/select2.css" media="screen"/>
    <utils:css url="/resources/css/application.css" media="screen"/>
    <utils:css url="/resources/datetime-picker/bootstrap-datetimepicker.min.css" media="screen"/>

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--[if lte IE 8]>
    <script>
        document.createElement('ng-include');
        document.createElement('ng-pluralize');
        document.createElement('ng-view');
        document.createElement('ng:include');
        document.createElement('ng:pluralize');
        document.createElement('ng:view');
    </script>
    <![endif]-->
    <!--[if lt IE 8]>
    <script src="//cdn.jsdelivr.net/json2/0.1/json2.min.js"></script>
    <![endif]-->
    <utils:script url="/resources/js/jquery-1.10.1.js"/>
    <utils:script url="/resources/js/underscore.js"/>
    <utils:script url="/resources/bootstrap/js/bootstrap.js"/>
    <utils:script url="/resources/js/flash.js"/>
    <utils:script url="/resources/select2/select2.js"/>
    <utils:script url="/resources/select2/select2_locale_pt-BR.js"/>
    <utils:script url="/resources/js/utils/jquery.maskMoney.js"/>
    <utils:script url="/resources/datetime-picker/bootstrap-datetimepicker.min.js"/>

    <utils:script url="/resources/angular/angular.js"/>
    <utils:script url="/resources/angular/ui-utils.js"/>
    <utils:script url="/resources/angular/select2.js"/>
    <utils:script url="/resources/angular/angular-bootstrap.js"/>
    <utils:script url="/resources/angular/angular-resource.min.js"/>
    <utils:script url="/resources/angular/i18n/angular-locale_pt.js"/>
    <utils:script url="/resources/angular/i18n/translations.js"/>
    <utils:script url="/resources/angular/i18n.js"/>
    <utils:script url="/resources/angular/pagingAndSorting.js"/>

    <utils:script url="/resources/js/utils/app-resource.js"/>
    <utils:script url="/resources/js/utils/default-inputs.js"/>
    <utils:script url="/resources/js/utils/currency-input.js"/>
    <utils:script url="/resources/js/utils/datetime-picker.js"/>
    <utils:script url="/resources/js/model/models.js"/>
    <utils:script url="/resources/js/controller/default-controller.js"></utils:script>
    <utils:script url="/resources/js/client-app.js"/>

    <base href="<c:url value="/" />" target="_blank"/>
</head>
<body>
<script type="text/javascript">
    var base_url = "<c:url value="/" />";
    var context_path = "${pageContext.request.contextPath}";
</script>

<div class="container">
    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <utils:a href="/" cssClass="brand">Teste</utils:a>
                <ul class="nav">
                    <sec:authorize access="hasRole('admin')">
                        <li><utils:a href="/users/">Usuários</utils:a></li>
                        <li><utils:a href="/clients/">Clientes</utils:a></li>
                        <li><utils:a href="/orders/">Pedidos</utils:a></li>
                    </sec:authorize>
                </ul>
                <ul class="nav pull-rigth">
                    <li><utils:a href="j_spring_security_logout" target="_self">sair</utils:a></li>
                    <li><utils:a href="/users/change_password" target="_self">mudar senha</utils:a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">

    <div class="row-fluid">
        <div class="alert hide" id="alert">
            <a class="close" href="#">&times;</a> <span></span>
        </div>

        <%@ taglib uri="http://www.springframework.org/security/tags"
                   prefix="sec" %>
        <sec:authorize access="isAnonymous()">
            <utils:a href="/users/">Gerenciar Usuários</utils:a>
        </sec:authorize>

        <div class="row-fluid">
            <div data-ng-view class="span12">
                <decorator:body/>
            </div>
        </div>
    </div>
</div>
</body>
</html>
