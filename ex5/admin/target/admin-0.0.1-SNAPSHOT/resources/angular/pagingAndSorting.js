(function(window, angular) {
'use strict';
var pagingAndSortingModule = angular.module('pagingAndSorting', ['ng']);

pagingAndSortingModule.factory('$pagingAndSorting', ['$rootScope', '$http', function($rootScope, $http){
    var DEFAULT_OPTIONS = {
        	url: '',
    		method: 'get',
    		property: 'page',
            success: function(){},
            error: function(){},
            page: 0,
            size: 10,
            sort: null,
            dir: 'asc'
        };
    
    function getPage(page){
    	var page = {
    		content: [],
		    firstPage: true,
		    lastPage: false,
		    number: 0,
		    numberOfElements: 0,
		    size: 0,
		    sort: null,
		    totalElements: 0,
		    totalPages: 0
    	};
    	return $.extend({}, page, page);
    };
    
    function PagingAndSortingFactory(options){
    	options = angular.extend({}, DEFAULT_OPTIONS, options);
    	var pageRequest = {
    			page: options.page,
                size: options.size,
                sort: options.sort,
                dir: options.dir
        	};
    	
    	var page = getPage({});
    	
    	var PagingAndSorting = {
    		'query': function(data){
    			$http({
    				method: options.method,
    				url: options.url + '?' + $.param(angular.extend(pageRequest, data)),
    			}).then(function(response){
    				var data = response.data;
    				page = data[options.property];
    				(options.success)(data, page, response.headers);
    			}, options.error);
    		},
    		'page': function(){
    			return page;
    		},
    		'pageRequest': function(){
    			return pageRequest;
    		}
    	};
    	
    	return PagingAndSorting;
    }
    
    return PagingAndSortingFactory;
}]);

pagingAndSortingModule.directive('sortColumn', function factory() {
    return {
        restrict: 'A',
        scope: { property: '=sortColumn', pagingAndSorting: '=pagingAndSorting' },
        link: function (scope, iEle, iAttrs) {
            function getSort() {
                angular.element(iEle).parent().find('i').remove();
                var sort = {
                	sort: scope.property,
                	dir: 'asc'
                };
                
                if(scope.property == scope.pagingAndSorting.pageRequest().sort){
                	if('asc' === scope.pagingAndSorting.pageRequest().dir)
                		sort.dir = 'desc';
                }

                if (sort.dir === 'asc') {
                    angular.element(iEle).prepend('<i class="icon-arrow-up"></i>');
                } else {
                    angular.element(iEle).prepend('<i class="icon-arrow-down"></i>');
                }
                return sort;
            }
            angular.element(iEle).click(function () {
                scope.pagingAndSorting.query(getSort());
            });
        }
    };
});

pagingAndSortingModule.directive('pages', function factory() {
    return {
        template: '<div class="pagination pagination-small"><ul></ul></div>',
        replace: true,
        restrict: 'E',
        scope: {pas: '=pagingAndSorting'},
        link: function (scope, iEle, iAttrs) {
            function update_buttons() {
                angular.element('ul', iEle).html('');
                for (var i = 0; i < scope.pas.page().totalPages; i++) {
                    var item = angular.element('<li><a href="#"></li>');
                    angular.element('a', item).text(i + 1).attr('data-page', i);

                    if (i === scope.pas.page().number)
                        angular.element(item).addClass('active').attr('disabled', 'disabled');
                    angular.element('ul', iEle).append(item);
                };

                angular.element(iEle).find('a').click(function (evt) {
                	evt.preventDefault();
                	evt.stopPropagation();
                    var page = angular.element(this).attr('data-page');
                    scope.pas.query({page: page});
                });
            }
            scope.$watch('pas.page()', function(newValue){
        		update_buttons();
            });
        }
    };
});

pagingAndSortingModule.directive('pageSize', function factory($pagingAndSorting, $compile) {
    return {
        template: '<div><select ng-model="page_size" style="width: 70px;"><option ng-repeat="i in [10, 25, 50]">{{i}}</option></select></div>',
        replace: true,
        restrict: 'E',
        scope: {pas: '=pagingAndSorting'},
        link: function (scope, iEle, iAttrs) {
        	scope.page_size = scope.pas.pageRequest().size;
            $compile(iEle);

            scope.$watch('page_size', function (newValue) {
                if (newValue) {
                    scope.pas.query({size: Number(newValue)});
                }
            });
        }
    };
});

})(window, window.angular);