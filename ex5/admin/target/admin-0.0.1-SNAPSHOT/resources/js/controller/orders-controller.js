var select2Client = {
    ajax: {
        url: base_url + 'clients.json',
        data: function(term){
            return {q: term}
        },
        results: function(data){
            var results = [];
            data = data.clientList.content;
            $.each(data, function(index, item){
                item.text = item.name;
                results.push(item);
            });
            return {results: results};
        }
    },
    formatSelection: function(item){
        return $('<span>').text(item.name);
    }
}

function OrderIndexCtrl(Order, $scope, $pagingAndSorting, $location, $routeParams, $locale, $filter) {
	return DefaultIndexCtrl(Order, 'order', $scope, $pagingAndSorting, $location, $routeParams, $locale, $filter);
}

function OrderEditCtrl(Order, $scope, $location, $routeParams, $locale, $filter, $http) {
    $scope.select2Client = select2Client;
    $scope.item = {orderId: $routeParams.id};
    $scope.isEdit = true;
    $scope.id = $routeParams.id;
	var ctrl =  DefaultEditCtrl(Order, 'order', $scope, $location, $routeParams, $locale, $filter);

    var save = $scope.save;
    $scope.save = function(){
        save();
        $location.path('/orders');
    };

    $scope.removeItem = function(item){
        if(item.id)
        {
            $http['delete'](base_url + 'order-items/'+item.id, function(){
                $location.path('/orders/'+$scope.id+'/edit');
            })
        }
        $location.path('/orders/'+$scope.id+'/edit');
    };

    return ctrl;
}

function OrderCreateCtrl(Order, $scope, $location, $routeParams, $locale, $filter) {
    $scope.select2Client = select2Client;
	return DefaultCreateCtrl(Order, 'order', '/orders', $scope, $location, $routeParams, $locale, $filter);
}