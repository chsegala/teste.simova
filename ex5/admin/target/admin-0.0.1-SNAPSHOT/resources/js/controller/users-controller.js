var select2Taxi = {
	ajax:{
		url: base_url + 'taxis.json',
		contentType: 'jsonp',
		data: function(term){
			return {q: term};
		},
		results: function(data){
			var results = [];
			data = data.taxiList.content;
			$.each(data, function(index, item){
				item.text = item.name;
				results.push(item);
			});
			return{results: results};
		}
	},
    initSelection: function(element, callback){
        $.get(base_url + 'taxis/' + $(element).val() + '.json').done(function(data){
            callback(data.taxi);
        });
    },
	formatSelection: function(item){
		return $('<span>').text(item.name);
	}
};

function UserIndexCtrl(User, $scope, $pagingAndSorting, $location, $routeParams, $locale,
		$filter) {	
	return DefaultIndexCtrl(User, 'user', $scope, $pagingAndSorting, $location,
			$routeParams, $locale, $filter);
}

function UserEditCtrl(User, Authority, $scope, $location, $routeParams, $locale, $filter) {
	$scope.isEdit = true;
	$scope.authorities = [];
	Authority.query({}, function(data){
		$scope.authorityList = data['authorityList'];
	});
	
	$scope.authority = function(value){
		if(!$scope.hasAuthority(value)){
			$scope.user.authorities.push(value);
		}else{
			var index = $scope.user.authorities.indexOf(_.findWhere($scope.user.authorities, {id: value.id}));
			$scope.user.authorities.splice(index, 1);
		}
	};
	
	$scope.hasAuthority = function(authority){
		return _.findWhere($scope.user.authorities, {id: authority.id}) != undefined;
	};
	
	$scope.select2Taxi = select2Taxi;

	var ctrl = DefaultEditCtrl(User, 'user', $scope, $location,
			$routeParams, $locale, $filter);

    var save = $scope.save;
    $scope.save = function(){
        if($scope.user.taxiId){
            $scope.user.taxiId = $scope.user.taxiId.id ;
        }
        save();
    };

    return ctrl;
}

function UserCreateCtrl(User, Authority, $scope, $location, $routeParams, $locale,
		$filter) {
	$scope.authorities = [];
	Authority.query({}, function(data){
		$scope.authorityList = data['authorityList'];
	});
	
	$scope.authority = function(value){
		if(!$scope.hasAuthority(value)){
			$scope.user.authorities.push(value);
		}else{
			var index = $scope.user.authorities.indexOf(_.findWhere($scope.user.authorities, {id: value.id}));
			$scope.user.authorities.splice(index, 1);
        }
	};
	
	$scope.hasAuthority = function(authority){
		return _.findWhere($scope.user.authorities, {id: authority.id}) != undefined;
	};
	
	$scope.select2Taxi = select2Taxi;
	
	var ctrl = DefaultCreateCtrl(User, 'user', '/users', $scope, $location,
			$routeParams, $locale, $filter);

    $scope.$watch('taxi', function(newValue){$scope.user.taxiId = newValue ? newValue.id : null});
    var save = $scope.save;
    $scope.save = function(){
        if($scope.user.taxiId){
            $scope.user.taxiId = $scope.user.taxiId.id ;
        }
        save();
    };

    return ctrl;
}

function UserRedefineCtrl($scope, $http, $location, $routeParams, $locale, $filter){
	$scope.save = function(){
		if($scope.password !== $scope.passwordConfirm){
			flash.error($filter('i18n')('Senha e confirmação incompatíveis'));
			return;
		}
		$http.put(base_url + 'users/' + $routeParams.id + '/redefine', $scope.password)
		.success(function(data){
			flash.success($filter('i18n')('Registro salvo'));
			$location.path('/users/');
		}).
		error(function(){
			flash.error($filter('i18n')('Erro ao salvar o registro'));
		});
	};
}

function UserChangePasswordCtrl($scope, $http, $location, $routeParams, $locale, $filter){
	$scope.save = function(){
		if($scope.password !== $scope.passwordConfirm){
			flash.error($filter('i18n')('Senha e confirmação incompatíveis'));
			return;
		}
		$http.put(base_url + 'users/change_password', {password: $scope.password, oldPassword: $scope.oldPassword})
		.success(function(data){
			flash.success($filter('i18n')('Registro salvo'));
		}).
		error(function(){
			flash.error($filter('i18n')('Erro ao salvar o registro'));
		});
	};
}