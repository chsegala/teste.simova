function ClientsIndexCtrl(Client, $scope, $pagingAndSorting, $location, $routeParams, $locale, $filter) {
	return DefaultIndexCtrl(Client, 'client', $scope, $pagingAndSorting, $location, $routeParams, $locale, $filter);
}

function ClientsEditCtrl(Client, $scope, $location, $routeParams, $locale, $filter) {
	return DefaultEditCtrl(Client, 'client', $scope, $location, $routeParams, $locale, $filter);
}

function ClientsCreateCtrl(Client, $scope, $location, $routeParams, $locale, $filter) {
	return DefaultCreateCtrl(Client, 'client', '/clients', $scope, $location, $routeParams, $locale, $filter);
}