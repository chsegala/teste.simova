var module = angular.module('currencyInput', ['ng']);
module.directive('currency', function($filter, $locale){
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, iEle, iAttr, ctrl){
			if(!iAttr.currency)
				iAttr.currency = 2;
			
			$(iEle).maskMoney({
				thousands: $locale['NUMBER_FORMATS']['GROUP_SEP'],
				decimal: $locale['NUMBER_FORMATS']['DECIMAL_SEP'],
				precision: Number(iAttr.currency),
				allowZero: true
			});
			
			ctrl.$parsers.push(function(string){
				if(!string) return '';
				string = Number(string.replace(/\D/gi, ''));
				return string / Math.pow(10, iAttr.currency);
			});
			
			$(iEle).bind('keyup', function(){
				scope.$apply(function(){
					var value = iEle.val();
					ctrl.$setViewValue(value);
				});
			});
		}
	};
});