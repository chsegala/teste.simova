var module = angular.module('datetimePicker', ['ng']);

module.directive('datetimePicker', function($locale, $filter){
	var DEFAULT_OPTIONS = {
			format: $locale["DATETIME_FORMATS"]["medium"].replace('HH', 'hh').replace(':ss', ''),
			language: $locale.id
		};
	
	return {
		restrict: 'A',
		require: 'ngModel',
		template: '<div class="input-append date"><input type="text" /><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span></div>',
		replace: true,
		compile: function(tEle, tAttr){
			return function(scope, iEle, iAttr, ctrl){
				var options = angular.extend({}, DEFAULT_OPTIONS, scope.$eval(iAttr.datetimePicker));
				var picker = $(iEle).datetimepicker(options);				
				iEle.find('input').attr('id', iEle.attr('id'));

				scope.$watch(function(){
					$(picker).data('datetimepicker').setDate(ctrl.$viewValue);
				});
				
				$(iEle).on('changeDate', function(e){
					scope.$apply(function(){
						ctrl.$setViewValue(e.date);
						console.log(e.date);
					});
				});
			};
		}
	};
});