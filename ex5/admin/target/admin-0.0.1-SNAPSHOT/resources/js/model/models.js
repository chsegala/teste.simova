modelsModule = angular.module('models', ['appResource']);

modelsModule.factory('Authority', ['appResource', function(appResource){
	return appResource(context_path + '/authorities/:id', {}, {});
}]);
modelsModule.factory('User', ['appResource', '$http', function($resource, $http){
	return $resource(context_path + '/users/:id/', {}, {});
}]);
modelsModule.factory('Client', ['appResource', '$http', function($resource, $http){
    return $resource(context_path + '/clients/:id/', {}, {});
}]);
modelsModule.factory('Order', ['appResource', '$http', function($resource, $http){
    return $resource(context_path + '/orders/:id/', {}, {});
}]);
modelsModule.factory('OrderItem', ['appResource', '$http', function($resource, $http){
    return $resource(context_path + '/order-items/:id/', {}, {});
}]);