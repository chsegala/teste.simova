package teste.simova.ex3;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/3/13
 * Time: 8:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class IdGenerator {
    private static AtomicLong id = new AtomicLong(1);
    private static IdGenerator instance = null;

    private IdGenerator(){

    }

    public long getId(){
        return id.getAndAdd(1);
    }

    public static IdGenerator getInstance(){
        if(instance == null)
            instance = new IdGenerator();
        return instance;
    }
}
