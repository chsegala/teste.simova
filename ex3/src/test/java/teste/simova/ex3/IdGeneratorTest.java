package teste.simova.ex3;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static junit.framework.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/3/13
 * Time: 8:54 PM
 * To change this template use File | Settings | File Templates.
 */

public class IdGeneratorTest {
    private int listSize = 1 * 1000;
    private List<Long> ids = Collections.synchronizedList(new ArrayList<Long>(listSize));

    private class IdGetterThread implements Runnable{
        @Override
        public void run() {
            long id = IdGenerator.getInstance().getId();
            ids.add(id);
        }
    }

    private class GetterThread implements Runnable{
        @Override
        public void run() {
            for(int i = 0; i < listSize; i++){
                new Thread(new IdGetterThread()).start();
            }
        }
    }


    @Test
    public void testGetId() throws Exception {
        new Thread(new GetterThread()).start();
        while (ids.size() < listSize)
            Thread.yield();

        assertEquals(listSize, ids.size());
        for(Long id : ids){
            assertEquals(1, Collections.frequency(ids, id));
        }

        Collections.sort(ids);
        for (int i = 0; i < listSize; i++){
            assertEquals(new Long(i + 1), ids.get(i));
        }
    }
}
