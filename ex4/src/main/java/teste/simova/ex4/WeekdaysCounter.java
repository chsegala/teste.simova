package teste.simova.ex4;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/3/13
 * Time: 9:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class WeekdaysCounter {
    public int weekdaysBetween(DateTime date1, DateTime date2){
        int daysBetween = 0;
        while (date1.isBefore(date2)){
            if(date1.dayOfWeek().get() != DateTimeConstants.SUNDAY && date1.dayOfWeek().get() != DateTimeConstants.SATURDAY)
                daysBetween++;

            date1 = date1.plusDays(1);
        }

        return daysBetween;
    }
}
