package teste.simova.ex4;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/3/13
 * Time: 8:02 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet(name = "simple-servlet", urlPatterns = {"/*"})
public class SimpleServlet extends HttpServlet {
    private DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String date1s = request.getParameter("date1");
        String date2s = request.getParameter("date2");

        if (StringUtils.isBlank(date1s) || StringUtils.isBlank(date2s)) {
            response.getWriter().write("Please, provide 2 dates named 'date1' and 'date2' as query string");
            return;
        }

        try {
            DateTime date1 = DateTime.parse(date1s, dateTimeFormatter);
            DateTime date2 = DateTime.parse(date2s, dateTimeFormatter);

            if(date2.isBefore(date1)){
                response.getWriter().write("The 'date2' must be greater than 'date1'");
                return;
            }

            int weekDays = new WeekdaysCounter().weekdaysBetween(date1, date2);
            response.getWriter().write(String.format("There are %d weekdays between dates", weekDays));


        } catch (Exception e) {
            response.getWriter().write("The dates should be in format dd/MM/yyyy");
        }
    }
}
