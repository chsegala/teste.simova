package teste.simova.ex4;

import org.joda.time.DateTime;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/3/13
 * Time: 9:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class WeekdaysCounterTest {
    @Test
    public void testCountWeekdays() throws Exception {
        WeekdaysCounter counter = new WeekdaysCounter();
        int weekdays = counter.weekdaysBetween(new DateTime(2013, 2, 1, 0, 0), new DateTime(2013, 2, 23, 0, 0));
        assertEquals(16, weekdays);

        weekdays = counter.weekdaysBetween(new DateTime(2013, 2, 2, 0, 0), new DateTime(2013, 3, 3, 0, 0));
        assertEquals(20, weekdays);
    }
}
