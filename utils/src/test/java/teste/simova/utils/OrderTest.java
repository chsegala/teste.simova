/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/3/13
 * Time: 5:55 PM
 * To change this template use File | Settings | File Templates.
 */

package teste.simova.utils;

import org.junit.Assert;
import org.junit.Test;

public class OrderTest {

    @Test
    public void parseTest(){
        String template = "Christian Segala\t111.111.111-11\t12-00009999";
        Order order = Order.parse(template);

        Assert.assertEquals("Christian Segala", order.getName());
        Assert.assertEquals("111.111.111-11", order.getCpf());
        Assert.assertEquals("12-00009999", order.getPhone());
    }
}
