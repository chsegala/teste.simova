package teste.simova.utils;

import junit.framework.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/3/13
 * Time: 6:01 PM
 * To change this template use File | Settings | File Templates.
 */

public class OrderItemTest {

    @Test
    public void parseTest(){
        String template = "ITEM 1\t200,05";
        OrderItem oi = OrderItem.parse(template);

        Assert.assertEquals("ITEM 1", oi.getName());
        Assert.assertEquals(new BigDecimal("200.05"), oi.getPrice());
    }
}
