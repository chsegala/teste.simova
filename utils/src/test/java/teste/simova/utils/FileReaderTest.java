package teste.simova.utils;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/3/13
 * Time: 6:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class FileReaderTest {
    @Test
    public void testParse() throws Exception {
        String template = "JoÃ£o da Silva\t006.086.334-55\t(12) 8888-9999\n" +
                    "Notebook HP 8GB\t2500,05\n" +
                    "Mouse sem fio Microsoft\t100,05\n" +
                    "Teclado sem fio Microsoft\t200,05\n" +
                    "Impressora HP\t500,05";

        InputStream is = new ByteArrayInputStream(template.getBytes());
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        Order order = FileReader.parse(br);

        assertEquals("JoÃ£o da Silva", order.getName());
        assertEquals("006.086.334-55", order.getCpf());
        assertEquals("(12) 8888-9999", order.getPhone());

        assertEquals("Notebook HP 8GB", order.getItems().get(0).getName());
        assertEquals(new BigDecimal("2500.05"), order.getItems().get(0).getPrice());

        assertEquals("Mouse sem fio Microsoft", order.getItems().get(1).getName());
        assertEquals(new BigDecimal("100.05"), order.getItems().get(1).getPrice());

        assertEquals("Teclado sem fio Microsoft", order.getItems().get(2).getName());
        assertEquals(new BigDecimal("200.05"), order.getItems().get(2).getPrice());

        assertEquals("Impressora HP", order.getItems().get(3).getName());
        assertEquals(new BigDecimal("500.05"), order.getItems().get(3).getPrice());

        assertEquals(new BigDecimal("3300.20"), order.getPrice());

    }
}
