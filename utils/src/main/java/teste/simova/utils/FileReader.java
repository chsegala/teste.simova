/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/3/13
 * Time: 5:31 PM
 * To change this template use File | Settings | File Templates.
 */
package teste.simova.utils;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;

public class FileReader {
    private FileReader(){}

    public static Order parse(BufferedReader reader) throws IOException {
        String line = null;
        int lineNumber = 0;
        Order order = null;
        while(!StringUtils.isBlank(line = reader.readLine())){
            if(lineNumber == 0)
                order = Order.parse(line);
            else
                order.getItems().add(OrderItem.parse(line));
            lineNumber++;
        }
        return order;
    }
}
