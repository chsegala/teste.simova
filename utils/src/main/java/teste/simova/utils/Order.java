package teste.simova.utils;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
* Created with IntelliJ IDEA.
* User: chsegala
* Date: 7/3/13
* Time: 5:48 PM
* To change this template use File | Settings | File Templates.
*/
@Data
public class Order {
    private String name;
    private String cpf;
    private String phone;

    private List<OrderItem> items = new ArrayList<OrderItem>();

    public BigDecimal getPrice(){
        BigDecimal price = new BigDecimal(0);
        for(OrderItem item : items){
            price = price.add(item.getPrice());
        }

        return price;
    }

    public static Order parse(String line) {
        Order o = new Order();
        String[] fields = StringUtils.split(line, '\t');
        o.setName(fields[0]);
        o.setCpf(fields[1]);
        o.setPhone(fields[2]);

        return o;
    }
}
