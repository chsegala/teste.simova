package teste.simova.utils;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

/**
* Created with IntelliJ IDEA.
* User: chsegala
* Date: 7/3/13
* Time: 5:48 PM
* To change this template use File | Settings | File Templates.
*/
@Data
public class OrderItem {
    private String name;
    private BigDecimal price;

    public static OrderItem parse(String line) {
        OrderItem oi = new OrderItem();
        String[] fields = StringUtils.split(line, '\t');

        oi.setName(fields[0]);
        oi.setPrice(new BigDecimal(fields[1].replace(',', '.')));

        return oi;
    }
}
