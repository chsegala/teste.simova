package teste.simova.ex1;

import teste.simova.utils.Order;
import teste.simova.utils.OrderItem;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: chsegala
 * Date: 7/3/13
 * Time: 6:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class FileImporter {
    public static void main(String... args){
        if(args.length < 1){
            System.out.println("You should provide a valid file as an argument");
            return;
        }

        String filename = args[0];

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
            Order order = teste.simova.utils.FileReader.parse(bufferedReader);
            printOrder(order, System.out);

        } catch (FileNotFoundException e) {
            System.out.println("Unable to find the file specified");
        } catch (IOException e) {
            System.out.println("Unable to read the file");
        }

    }

    public static void printOrder(Order order, PrintStream writer) {
        writer.println(String.format("Nome: %s", order.getName()));
        writer.println(String.format("CPF: %s", order.getCpf()));
        writer.println(String.format("Tel: %s", order.getPhone()));

        for (int i = 0; i < order.getItems().size(); i++){
            OrderItem oi = order.getItems().get(i);
            writer.println(String.format("\t %d. Item:%s - R$ %.2f", i, oi.getName(), oi.getPrice().doubleValue()));
        }

        writer.println(String.format("\n\nTotal: R$ %.2f", order.getPrice().doubleValue()));
    }
}
